__author__ = 'RogarM13'

from tkinter import *
from math import *
from random import *

class Tetris():



    def __init__(self,master):

        menu = Menu(master)
        master.config(menu=menu) # Dodamo menu

        igra_menu = Menu(menu)
        menu.add_cascade(label="Igra", menu=igra_menu)
        igra_menu.add_command(label="Nova igra",command= self.nova_igra)
        igra_menu.add_command(label="Končaj igro", command = lambda: 1+1)
        igralci_menu = Menu(menu)
        menu.add_cascade(label="Igralci",menu=igralci_menu)
        igralci_menu.add_command(label="Človek",command = lambda:1+1)
        igralci_menu.add_command(label="Računalnik",command = lambda:1+1)



        self.canvas = Canvas(master,width=500,height=750,bg="lightblue")
        self.canvas.grid(row=1,column=1,columnspan=10,rowspan=15)



        for i in range(10):
            self.canvas.create_line(i*50,0,i*50,750)
        for i in range(15):
            self.canvas.create_line(0,i*50,500,i*50)

        self.napis = StringVar(master,value="Da vidimo kaj zmoreš!!")
        Label(master, textvariable=self.napis).grid(row=0, column=2)
        self.točkovanje = StringVar(master,value = 0 )
        Label(master, textvariable=self.točkovanje).grid(row=0,column=10,columnspan=2)

        self.time = Label(master, fg='black')
        self.time.grid(row=0,column=8)

        self.button=Button(text="Začni igro",command=lambda: 1+1)
        self.button.grid(row=0,column=1)

        self.polje=[[0 for i in range(10)] for i in range(15)]
        #vsi možni elementi in njihove možnosti za levo, desno, 4. komponenta pove, koliko desno/v smeri x, lahko gremo
        #(levo,el.,desno,x)

        self.elementi=[(1,1,1,8),(3,2,3,9),(2,3,3,6),(5,4,5,7),(4,5,4,8),(7,6,7,7),(6,7,6,8),
                (9,8,10,7),(11,9,8,8),(8,10,11,8),(10,11,9,7),(13,12,15,8),(14,13,12,7),(15,14,13,8),(12,15,14),
                (17,16,19,8),(18,17,16,7),(19,18,17,8),(16,19,18,7)]
        #vsakemu elementu priredimo 4x4 matriko z vrednostmi 1 in 0.




        #id grafičnih elementov
        self.id_elementov=[]


    #pobriše vrstico če je le ta polna
    def pobrisi(self):

        for i in range(len(self.polje)):
            if sum(self.polje[i]) == 10:
                for j in range(10):
                    self.polje[i][j] = 0
                    self.canvas.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50,fill="lightblue")

                    self.točkovanje.set(+1)




    #nova igra
    def nova_igra(self):

        self.polje = [[0 for i in range(10)] for i in range(15)]
        self.canvas.delete(ALL)
        for i in range(10):
            self.canvas.create_line(i*50,0,i*50,750)
        for i in range(15):
            self.canvas.create_line(0,i*50,500,i*50)
        self.napis.set("Da vidimo kaj zmoreš")

        self.točkovanje.set(0)

    cas = 0
    #element, ki se je zgoraj pojavil, pada če lahko
    def padanje(self):
        global cas
        cas += 1
        self.time["text"] = cas
        self.time.after(1000,self.padanje)
        if cas == 10:
            cas = 0
            for i in range(len(self.polje)):
                for j in range(len(self.polje[i])):
                    if self.polje[i][j] == 10 and self.polje[i+1][j]!= 10:
                        self.canvas.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50,fill="lightblue")
                        self.canvas.create_rectangle(j*50,(i+1)*50,(j+1)*50,(i+2)*50,fill="red")

root = Tk()

root.title("Tetris")

aplikacija=Tetris(root)


root.mainloop()