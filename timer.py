from tkinter import *

root = Tk()
root.title('Timer')
root.state('zoomed')

sec = 0
platno=Canvas(root,width=200,height=1000,bg="lightgreen")
platno.grid(row=2,column=2)
def tick():
    global sec
    sec += 1
    time['text'] = sec
    # Take advantage of the after method of the Label
    time.after(1000, tick)
    if sec==10:
        sec=0
    platno.create_rectangle(50,sec*15,100,sec*15+10,fill="red")
    platno.create_rectangle(50,(sec-1)*15,100,(sec-1)*15+10,fill="lightgreen",outline="lightgreen")
    platno.create_rectangle(50,(10)*15,100,(10)*15+10,fill="lightgreen",outline="lightgreen")

time = Label(root, fg='green')
time.grid(row=1,column=1)
Button(root, fg='blue', text='Start', command=tick).grid(row=0,column=1)

root.mainloop()
